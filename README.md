# Dev Env

When you open this in VSCode, the skilltree service will be running on port 8080.

Once the container is running, go to [localhost port 8080 to get started](localhost:8080).

[Skilltree docs](https://code.nsa.gov/skills-docs/overview/)