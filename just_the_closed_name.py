#!/usr/bin/env python3
import argparse
import requests

from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

gl = requests.Session()
retries = Retry(
    total=5,
    backoff_factor=1,
    status_forcelist=[401, 502, 503, 504])
gl.mount('https://', HTTPAdapter(max_retries=retries))

gitlab_api = 'https://gitlab.com/api/v4'
gitlab_projects_api = f'{gitlab_api}/projects'
gitlab_groups_api = f'{gitlab_api}/groups'

def get_all_issues (project_id: int, token: str):
    '''
    Return a list of issue objects returned by
    https://docs.gitlab.com/ee/api/issues.html#list-project-issues
    '''
    issue_list = []
    curr_list = []
    page = 1
    print('\tGetting project issues...')
    while len(curr_list) > 0 or page == 1:
        res = gl.get(
            f'{gitlab_projects_api}/{project_id}/issues?scope=all&'
            f'per_page=100&page={page}',
            headers={'PRIVATE-TOKEN': token})
        if res is not None:
            curr_list = res.json()
            issue_list += curr_list
        else:
            curr_list = []
        page += 1
    return issue_list

def list_closed_issues(issues: list):
    '''
        Returns all issues that are:
          1. Closed
    '''
    return [issue
            for issue in issues
            if issue['state'] == "closed"
            ]

def get_the_name(issues: list):
    return [issue['title']
            for issue in issues
            ]


def main():
    parser = argparse.ArgumentParser(
                        description='List closed issues in repo')
    parser.add_argument('-p', '--project-id', type=int, required=True,
                        help='specify project-id for the'
                             ' member training repo project.')
    parser.add_argument('token', type=str,
                        help='gitlab private token of account with 90COS'
                             ' access to member training repo group')
    parsed_args = parser.parse_args()
    project_id = parsed_args.project_id
    token = parsed_args.token

    issues = get_all_issues(project_id, token)
    closed_issues = list_closed_issues(issues)
    names_only =  get_the_name(closed_issues)


    print (names_only)



if __name__ == "__main__":
    main()
